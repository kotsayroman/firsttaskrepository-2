using System;

namespace Matrix
{
	// This class represents mathematical matrix with basic operations on it.
	class Matrix : IComparable<Matrix>
	{
		private int _rows;
		private int _columns;
		private double[,] _data;
		
		#region Constructors
		// Creates matrix with specified number of rows and columns.
		public Matrix(int rows, int columns)
		{
			_rows = rows;
			_columns = columns;
			_data = new double[rows, columns];
		}
		
		// Creates matrix with specified number of rows and columns, and fills matrix elements.
		public Matrix(int rows, int columns, double[,] data)
		{
			_rows = rows;
			_columns = columns;
			_data = new double[rows, columns];
			if (rows * columns != data.Length)
			{
				throw new ArgumentException("Bad array", "data");
			}
			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					_data[i, j] = data[i, j];
				}
			}
		}
		
		// Creates matrix with specified number of rows and columns, and fills matrix with concrete element.
		public Matrix(int rows, int columns, double value)
		{
			_rows = rows;
			_columns = columns;
			_data = new double[rows, columns];
			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					_data[i, j] = value;
				}
			}
		}
		
		#endregion
		
		#region Properties
		
		// Gets number of rows.
		public int Rows
		{
			get
			{
				return _rows;
			}
		}
		
		// Gets number of columns.
		public int Columns
		{
			get
			{
				return _columns;
			}
		}
		
		// Gets and sets specified element in matrix.
		public double this[int i, int j]
		{
			get
			{
				if (i < 0 || i >= _rows || j < 0 || j >= _columns)
				{
					throw new ArgumentOutOfRangeException();
				}
				return _data[i, j];
			}
			set
			{
				if (i < 0 || i >= _rows || j < 0 || j >= _columns)
				{
					throw new ArgumentOutOfRangeException();
				}
				_data[i, j] = value;
			}
		}
		#endregion
		
		#region Methods
		
		// Returns string representation of matrix.
		public override string ToString()
		{
			string mstrixString = "";
			
			for (int i = 0; i < _rows; i++)
			{
				for (int j = 0; j < _columns; j++)
				{
					mstrixString += String.Format("{0}\t", _data[i, j]);
				}
				mstrixString += "\n";
			}
			return mstrixString;
		}
		
		// Returns true if matrices have equal elements.
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			if (this.GetType() != obj.GetType())
			{
				return false;
			}
			Matrix other = (Matrix)obj;
			if (this._rows != other._rows || this._columns != other._columns)
			{
				return false;
			}
			for (int i = 0; i < _rows; i++)
			{
				for (int j = 0; j < _columns; j++)
				{
					if (_data[i, j] != other._data[i, j])
					{
						return false;
					}
				}
			}
			return true;
		}
		
		// Returns hash code value for the matrix.
		public override int GetHashCode()
		{
			return this.ToString().GetHashCode();
		}
		
		// Returns new matrix that represents sum of two other.
		// Matrices must have the same number of columns and rows.
		public static Matrix Add(Matrix lhsMatrix, Matrix rhsMatrix)
		{
			if (lhsMatrix._rows != rhsMatrix._rows || lhsMatrix._columns != rhsMatrix._columns)
			{
				throw new ArgumentException("Different size of matrices", "matrix");
			}
			Matrix additionResult = new Matrix(lhsMatrix._rows, lhsMatrix._columns);
			for (int i = 0; i < lhsMatrix._rows; i++)
			{
				for (int j = 0; j < lhsMatrix._columns; j++)
				{
					additionResult._data[i, j] = lhsMatrix._data[i, j] + rhsMatrix._data[i, j];
				}
			}
			return additionResult;
		}
		
		// Returns new matrix that represents difference of two other.
		// Matrices must have the same number of columns and rows.
		public static Matrix Subtract(Matrix lhsMatrix, Matrix rhsMatrix)
		{
			if (lhsMatrix._rows != rhsMatrix._rows || lhsMatrix._columns != rhsMatrix._columns)
			{
				throw new ArgumentException("Different size of matrices", "matrix");
			}
			Matrix substrResult = new Matrix(lhsMatrix._rows, lhsMatrix._columns);
			for (int i = 0; i < lhsMatrix._rows; i++)
			{
				for (int j = 0; j < lhsMatrix._columns; j++)
				{
					substrResult._data[i, j] = lhsMatrix._data[i, j] - rhsMatrix._data[i, j];
				}
			}
			return substrResult;
		}
		
		// Returns new matrix that represents product of two other.
		// Number of columns in first matrix must be equal to number of rows in second matrix.
		public static Matrix Multiply(Matrix lhsMatrix, Matrix rhsMatrix)
		{
			if (lhsMatrix._columns != rhsMatrix._rows)
			{
				throw new ArgumentException("Incorrect size of matrices", "matrix");
			}
			Matrix multResult = new Matrix(lhsMatrix._rows, rhsMatrix._columns);
			for (int i = 0; i < lhsMatrix._rows; i++)
			{
				for (int j = 0; j < rhsMatrix._columns; j++)
				{
					for (int k = 0; k < lhsMatrix._columns; k++)
					{
						multResult._data[i, j] += lhsMatrix._data[i, k] * rhsMatrix._data[k, j];
					}
				}
			}
			return multResult;
		}
		
		// Returns new matrix that represents another matrices multiplied by scalar.
		public static Matrix Multiply(Matrix matrix, double multiplier)
		{
			Matrix multResult = new Matrix(matrix._rows, matrix._columns);
			for (int i = 0; i < matrix._rows; i++)
			{
				for (int j = 0; j < matrix._columns; j++)
				{
					multResult._data[i, j] = matrix._data[i, j] * multiplier;
				}
			}
			return multResult;
		}
		
		// Returns new matrix that represents transposed matrix specified in arguments.
		public static Matrix Transpose(Matrix matrix)
		{
			Matrix transposeMatrix = new Matrix(matrix._columns, matrix._rows);
			for (int i = 0; i < matrix._rows; i++)
			{
				for (int j = 0; j < matrix._columns; j++)
				{
					transposeMatrix._data[j, i] = matrix._data[i, j];
				}
			}
			return transposeMatrix;
		}
		
		// Returns new matrix that consist of cows and columns selected from another matrix.
		public static Matrix Submatrix(Matrix matrix, int[] remainingRows, int[] remainingColumns)
		{		
			Matrix submatrix = new Matrix(remainingRows.Length, remainingColumns.Length);
			for (int i = 0; i < remainingRows.Length; i++)
			{
				for (int j = 0; j < remainingColumns.Length; j++)
				{ 
					submatrix._data[i, j] = matrix._data[remainingRows[i], remainingColumns[j]];
				}
			}
			return submatrix;
		}
		
		// Returns int that represents comparison of two matrices.
		// Compares sum of elements absolute value.
		public int CompareTo(Matrix otherMatrix)
		{
			return (this.absSumOfElements()).CompareTo(otherMatrix.absSumOfElements());
		}
		#endregion
		
		#region Operators
		
		// Matrix addition.
		public static Matrix operator + (Matrix lhsMatrix, Matrix rhsMatrix)
		{
			return Add(lhsMatrix, rhsMatrix);
		}
		
		// Matrix subtraction.
		public static Matrix operator - (Matrix lhsMatrix, Matrix rhsMatrix)
		{
			return Subtract(lhsMatrix, rhsMatrix);
		}
		
		// Matrix multiplication.
		public static Matrix operator * (Matrix lhsMatrix, Matrix rhsMatrix)
		{
			return Multiply(lhsMatrix, rhsMatrix);
		}
		
		// Scalar multiplication.
		public static Matrix operator * (Matrix lhsMatrix, double multiplier)
		{
			return Multiply(lhsMatrix, multiplier);
		}
		
		// Scalar multiplication.
		public static Matrix operator * (double multiplier, Matrix rhsMatrix)
		{
			return Multiply(rhsMatrix, multiplier);
		}
		
		// Operator <
		public static bool operator < (Matrix lhsMatrix, Matrix rhsMatrix)
		{
			return lhsMatrix.CompareTo(rhsMatrix) < 0;
		}
		
		// Operator >
		public static bool operator > (Matrix lhsMatrix, Matrix rhsMatrix)
		{
			return lhsMatrix.CompareTo(rhsMatrix) > 0;
		}
		
		// Operator <=
		public static bool operator <= (Matrix lhsMatrix, Matrix rhsMatrix)
		{
			return lhsMatrix.CompareTo(rhsMatrix) <= 0;
		}
		
		// Operator >=
		public static bool operator >= (Matrix lhsMatrix, Matrix rhsMatrix)
		{
			return lhsMatrix.CompareTo(rhsMatrix) >= 0;
		}
		
		// Operator ==
		// Based on comparison of elements sum.
		public static bool operator == (Matrix lhsMatrix, Matrix rhsMatrix)
		{
			return lhsMatrix.CompareTo(rhsMatrix) == 0;
		}
		
		// Operator !=
		public static bool operator != (Matrix lhsMatrix, Matrix rhsMatrix)
		{
			return lhsMatrix.CompareTo(rhsMatrix) != 0;
		}
		
		#endregion
		
		#region PrivateMethods
		private double absSumOfElements()
		{
			double sum = 0;
			
			for (int i = 0; i < _rows; i++)
			{
				for (int j = 0; j < _columns; j++)
				{
					sum += Math.Abs(_data[i, j]);
				}
			}
			return sum;
		}
		#endregion
	}
	
	class Program
	{
		static void Main()
		{
			Console.WriteLine("\n\t Fun with matrices!!!\n");
		
			Matrix matrix1 = new Matrix(3, 4, new double[3, 4]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}});
			
			Console.WriteLine("matrix1 [{0} {1}]: \n{2}", matrix1.Rows, matrix1.Columns, matrix1);
			Console.WriteLine();
			
			Matrix matrix2 = new Matrix(4, 5, 1);
			Console.WriteLine("matrix2 [{0} {1}]: \n{2}", matrix2.Rows, matrix2.Columns, matrix2);
			Console.WriteLine();
			
			Matrix matrix3 = new Matrix(3, 4, 1);
			Console.WriteLine("matrix3 [{0} {1}]: \n{2}", matrix3.Rows, matrix3.Columns, matrix3);
			Console.WriteLine();
			try
			{
				Console.WriteLine(" matrix1 + matrix2 : \n{0}", matrix1 + matrix2);
			}
			catch (ArgumentException e)
			{
				Console.WriteLine(e.Message);
			}
			Console.WriteLine();
			
			Console.WriteLine(" matrix1 - matrix3 : \n{0}", matrix1 - matrix3);
			Console.WriteLine();
			
			Console.WriteLine(" matrix3 * 2 : \n{0}", matrix3 * 2);
			Console.WriteLine();
			
			Console.WriteLine(" matrix1 * matrix2 : \n{0}", matrix1 * matrix2);
			Console.WriteLine();
			
			Console.WriteLine(" Transpose matrix1 : \n{0}", Matrix.Transpose(matrix1) );
			Console.WriteLine();
			
			Console.WriteLine(" Select rows 1, 2 and columns 2, 3, 4 from matrix1 : \n{0}", 
									Matrix.Submatrix(matrix1, new int[]{0, 1}, new int[]{1, 2, 3}));
			Console.WriteLine();
			
			Console.WriteLine("matrix1.Equals(matrix2) :   {0}", matrix1.Equals(matrix2));
			Console.WriteLine("matrix1 == matrix2 :   {0}", matrix1 == matrix2);
			Console.WriteLine("matrix1 != matrix2 :   {0}", matrix1 != matrix2);
			Console.WriteLine("matrix1 > matrix2 :   {0}", matrix1 > matrix2);
			Console.WriteLine("matrix1 < matrix2 :   {0}", matrix1 < matrix2);
		}
	}
}